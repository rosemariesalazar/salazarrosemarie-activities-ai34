<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalInfo extends Model
{
    use HasFactory;
    public $guarded = [];

    public function student(){
        return $this->belongsTo(StudentInfo::class, 'student_info_id', 'id');
    }

}
