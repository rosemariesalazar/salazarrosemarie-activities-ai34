<?php

namespace App\Http\Controllers;

use App\Models\MedicalInfo;
use App\Models\StudentInfo;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {
        $students = StudentInfo::get();
        $student_count = StudentInfo::count();
        $medical_records =    MedicalInfo::with(['student'])->get();
        $medical_record_count =    MedicalInfo::with(['student'])->count();

     
        return response()->json(['students' => $students, 'student_count' => $student_count, 
        'medical_records' => $medical_records, 'medical_record_count' => $medical_record_count]);
    }

    public function store(Request $request) {
        
        $this->validate($request, [
            'student_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'address' => 'required',
            'age' => 'required',
            'contact_number' => 'required',
            'civil_status' => 'required',
            'gender' => 'required',
        ]);

        $data = [
            'student_id' => $request->student_id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'age' => $request->age,
            'contact_number' => $request->contact_number,
            'civil_status' => $request->civil_status,
            'gender' => $request->gender,
        ];

        StudentInfo::create($data);

        return response()->json(['message' => 'New Student added.'], 200);

    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'student_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'address' => 'required',
            'age' => 'required',
            'contact_number' => 'required',
            'civil_status' => 'required',
            'gender' => 'required',
        ]);

        $data = [
            'student_id' => $request->student_id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'age' => $request->age,
            'contact_number' => $request->contact_number,
            'civil_status' => $request->civil_status,
            'gender' => $request->gender,
        ];
        
        StudentInfo::where('id', $id)->update($data);
        return response()->json(['message' => 'Student Information updated.'], 200);

    }

    public function storeMedicalRecord(Request $request) {

        $this->validate($request, [
            'student_info_id' => 'required',
            'complaint' => 'required',
            'has_asthma' => 'required',
            'has_diabetes' => 'required',
            'has_seizures' => 'required',
            'pulse_rate' => 'required',
            'blood_rate' => 'required',
            'blood_pressure' => 'required',
            'temperature' => 'required',
            'illness_history' => 'required',
            'treatment' => 'required',
            'physical_examination' => 'required',
            'diagnosis' => 'required',
        ]);

        $data = [
            'student_info_id' => $request->student_info_id,
            'complaint' => $request->complaint,
            'has_asthma' => $request->has_asthma,
            'has_diabetes' => $request->has_diabetes,
            'has_seizures' => $request->has_seizures,
            'pulse_rate' => $request->pulse_rate,
            'blood_rate' => $request->blood_rate,
            'blood_pressure' => $request->blood_pressure,
            'temperature' => $request->temperature,
            'illness_history' => $request->illness_history,
            'treatment' => $request->treatment,
            'physical_examination' => $request->physical_examination,
            'diagnosis' => $request->diagnosis,
        ];

        MedicalInfo::create($data);

        return response()->json(['message' => 'New Medical Record added.'], 200);

    }
    public function updateMedicalRecord(Request $request, $id) {

        $this->validate($request, [
            'student_info_id' => 'required',
            'complaint' => 'required',
            'has_asthma' => 'required',
            'has_diabetes' => 'required',
            'has_seizures' => 'required',
            'pulse_rate' => 'required',
            'blood_rate' => 'required',
            'blood_pressure' => 'required',
            'temperature' => 'required',
            'illness_history' => 'required',
            'treatment' => 'required',
            'physical_examination' => 'required',
            'diagnosis' => 'required',
        ]);

        $data = [
            'student_info_id' => $request->student_info_id,
            'complaint' => $request->complaint,
            'has_asthma' => $request->has_asthma,
            'has_diabetes' => $request->has_diabetes,
            'has_seizures' => $request->has_seizures,
            'pulse_rate' => $request->pulse_rate,
            'blood_rate' => $request->blood_rate,
            'blood_pressure' => $request->blood_pressure,
            'temperature' => $request->temperature,
            'illness_history' => $request->illness_history,
            'treatment' => $request->treatment,
            'physical_examination' => $request->physical_examination,
            'diagnosis' => $request->diagnosis,
        ];

        MedicalInfo::where('id', $id)->update($data);

        return response()->json(['message' => 'Medical Record updated.'], 200);

    }
}
