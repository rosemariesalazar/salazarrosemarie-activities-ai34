<?php

namespace App\Http\Controllers;

use App\Models\MedicalInfo;
use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'store']]);
    }

    public function getData(){
        $medical_infos = MedicalInfo::where('id', auth()->user()->id)->get();
        $medical_info_count = MedicalInfo::where('id', auth()->user()->id)->count();
        return response()->json(['medical_infos' => $medical_infos, 'medical_info_count' => $medical_info_count]);
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'student_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'address' => 'required',
            'age' => 'required',
            'contact_number' => 'required',
            'civil_status' => 'required',
            'gender' => 'required',
        ]);

        $data = [
            'student_id' => $request->student_id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'age' => $request->age,
            'contact_number' => $request->contact_number,
            'civil_status' => $request->civil_status,
            'gender' => $request->gender,
        ];

        StudentInfo::where('id', $id)->update($data);
        $user = User::where('id', auth()->user()->id)->first();

        $data = ['email' => $request->email];

        if($request->password){
            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);
        return response()->json(['message' => 'Student Information updated.'], 200);

    }
}
