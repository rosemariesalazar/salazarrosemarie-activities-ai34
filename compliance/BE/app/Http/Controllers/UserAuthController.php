<?php

namespace App\Http\Controllers;

use App\Models\StudentInfo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'store']]);
    }
    
    public function login(Request $request)
    {

        if (! $token = auth()->guard('api')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
            'student_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'address' => 'required',
            'age' => 'required',
            'contact_number' => 'required',
            'civil_status' => 'required',
            'gender' => 'required',
            'email' => 'unique:users,email'
        ]);
        $data = [
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'contact_number' => $request->contact_number,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'civil_status' => $request->civil_status,
            'age' => $request->age,
            'gender' => $request->gender,
            'student_id' => $request->student_id,
        ];
        
        $user = StudentInfo::create($data);

        User::create([
            'student_info_id' => $user->id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['success' => 'Account created successfuly!'], 200);
    }

    public function me()
    {
        return response()->json(User::with(['userinfo'])->first());
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }
}
