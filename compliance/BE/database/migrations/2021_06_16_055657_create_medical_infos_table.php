<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_info_id')->constrained('student_infos')->onUpdate('cascade')->onDelete('cascade');
            $table->string('complaint');
            $table->boolean('has_asthma');
            $table->boolean('has_diabetes');
            $table->boolean('has_seizures');
            $table->string('pulse_rate');
            $table->string('blood_rate');
            $table->string('blood_pressure');
            $table->string('temperature');
            $table->string('illness_history');
            $table->string('treatment');
            $table->string('physical_examination');
            $table->text('diagnosis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_infos');
    }
}
