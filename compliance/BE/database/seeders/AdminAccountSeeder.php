<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'first_name' => 'Rose Marie',
            'middle_name' => 'Sabalza',
            'last_name' => 'Salazar',
            'gender' => 'Female',
            'email' => 'rosemarie@gmail.com',
            'password' => Hash::make('123123')
        ]);
    }
}
