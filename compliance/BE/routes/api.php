<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserAuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminAuthController::class, 'login']);
        Route::post('logout', [AdminAuthController::class, 'logout']);
        Route::post('me', [AdminAuthController::class, 'me']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('store', [UserAuthController::class, 'store']);
        Route::put('updateAccount', [UserAuthController::class, 'updateAccount']);
        Route::post('login', [UserAuthController::class, 'login']);
        Route::post('logout', [UserAuthController::class, 'logout']);
        Route::post('me', [UserAuthController::class, 'me']);
    });
});

Route::group(['middleware' => 'api', 'prefix' => 'admin'], function () {
    Route::get('index', [AdminController::class, 'index']);
    Route::post('store', [AdminController::class, 'store']);
    Route::put('update/{id}', [AdminController::class, 'update']);
    Route::post('storeMedicalRecord', [AdminController::class, 'storeMedicalRecord']);
    Route::put('updateMedicalRecord/{id}', [AdminController::class, 'updateMedicalRecord']);
});

Route::group(['middleware' => 'api', 'prefix' => 'user'], function () {
    Route::get('index', [UserController::class, 'getData']);
    Route::put('update/{id}', [UserController::class, 'update']);
});
