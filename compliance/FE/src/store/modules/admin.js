import axios from '@/axios/'

export default {
    namespaced: true,
    state: {
        students: [],
        student_count: 0,
        medical_records: [],
        medical_record_count: 0
    },
    mutations: {
        SET_STUDENTS(state, students) {
            state.students = students;
        },
        SET_STUDENT_COUNT(state, count) {
            state.student_count = count
        },
        SET_MEDICAL_RECORDS(state, medical_records) {
            state.medical_records = medical_records;
        },
        SET_MEDICAL_RECORD_COUNT(state, count) {
            state.medical_record_count = count
        },
    },
    getters: {
        GET_USER(state) {
            return state.user;
        },
        GET_STUDENTS(state) {
            return state.students;
        },
        GET_STUDENT_COUNT(state) {
            return state.student_count;
        }
    },
    actions: {
        async GET_STUDENTS({ commit }) {
            const res = await axios.get(`admin/index`)
                .then((response) => {
                    commit("SET_STUDENTS", response.data.students);
                    commit("SET_STUDENT_COUNT", response.data.student_count);
                    commit("SET_MEDICAL_RECORDS", response.data.medical_records);
                    commit("SET_MEDICAL_RECORD_COUNT", response.data.medical_record_count);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async GET_MEDICAL_RECORDS({ commit }) {
            const res = await axios.get(`admin/index`)
                .then((response) => {
                    commit("SET_MEDICAL_RECORDS", response.data.medical_records);
                    commit("SET_MEDICAL_RECORD_COUNT", response.data.medical_record_count);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async STORE_STUDENT({ commit }, data) {
            const res = await axios.post(`admin/store`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async STORE_MEDICAL_RECORD({ commit }, data) {
            const res = await axios.post(`admin/storeMedicalRecord`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async UPDATE_STUDENT({ commit }, {data, id}) {
            const res = await axios.put(`admin/update/${id}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async UPDATE_MEDICAL_RECORD({ commit }, {data, id}) {
            const res = await axios.put(`admin/updateMedicalRecord/${id}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        }
    }
}