import axios from '@/axios/'
const AUTH = "auth";

export default {
    namespaced: true,
    state: {
        token: localStorage.getItem("auth") || "",
        user: []
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
            const BEARER_TOKEN = localStorage.getItem("auth") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_TOKEN(state, token) {
            localStorage.setItem("auth", token);
            localStorage.setItem('isUser', 'true')

            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("auth") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_ADMIN_TOKEN(state, token) {
            localStorage.setItem("auth", token);
            localStorage.setItem('isAdmin', 'true')

            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("auth") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_USER_TOKEN(state, token) {
            localStorage.setItem("auth", token);
            localStorage.setItem('isUser', 'true')

            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("auth") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_USER(state) {
            localStorage.removeItem("auth");
            localStorage.removeItem("isAdmin");
            localStorage.removeItem("isUser");
            state.token = "";

            axios.defaults.headers.common["Authorization"] = "";
        },
    },
    getters: {
        GET_USER(state) {
            return state.user;
        },
        GET_TOKEN(state) {
            return state.token;
        },
    },
    actions: {
        async SIGNUP({ commit }, data) {
            const res = await axios.post(`${AUTH}/user/store`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async ADMINLOGIN({ commit }, user) {
            const res = await axios.post(`${AUTH}/admin/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_ADMIN_TOKEN", response.data.access_token);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async STUDENTLOGIN({ commit }, user) {
            const res = await axios.post(`${AUTH}/user/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_USER_TOKEN", response.data.access_token);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async CHECK_ADMIN({ commit }) {
            const res = await axios.post(
                `${AUTH}/admin/me?token=` + localStorage.getItem("auth")
            )
                .then((response) => {
                    commit("SET_USER", response.data);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async CHECK_STUDENT({ commit }) {
            const res = await axios.post(
                `${AUTH}/user/me?token=` + localStorage.getItem("auth")
            )
                .then((response) => {
                    commit("SET_USER", response.data);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async LOGOUT_ADMIN({ commit }) {
            const res = await axios.post(`${AUTH}/admin/logout?token=` + localStorage.getItem("auth"))
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async LOGOUT_USER({ commit }) {
            const res = await axios.post(`${AUTH}/user/logout?token=` + localStorage.getItem("auth"))
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async UPDATE_ACCOUNT({ commit }, {data, id}) {
            const res = await axios.put(`user/update/${id}?token=` + localStorage.getItem("auth"), data)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}