import axios from '@/axios/'

export default {
    namespaced: true,
    state: {
        med_records: {},
        med_record_count: {}

    },
    mutations: {
        SET_MEDICAL_RECORDS(state, data){
            state.med_records = data
        },
        SET_MEDICAL_RECORD_COUNT(state, med_record_count){
            state.med_record_count = med_record_count
        }
    },
    getters: {

    },
    actions: {
        async MEDICAL_RECORD({ commit }) {
            const res = await axios.get('user/index')
                .then((response) => {
                    commit("SET_MEDICAL_RECORDS", response.data.medical_infos);
                    commit("SET_MEDICAL_RECORD_COUNT", response.data.medical_info_count);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async SAVE_UPDATE({ commit }, {data, id}){
            const res = await axios.put(`user/update/${id}`, data).then(res => {
                return res;
            }).catch(err => {
                return err.response
            });
            return res;
        }
    }
}