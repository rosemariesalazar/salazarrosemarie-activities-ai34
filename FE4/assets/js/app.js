
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [10, 9, 5, 11, 8, 4],
      backgroundColor: 'rgba(0, 0, 0, .8)',
    },
    {
      label: 'Returned Books',
      data: [11, 8, 10, 8, 5, 8],
      backgroundColor: 'rgba(179, 179, 179, .8)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [30, 10, 25, 20, 15],
        backgroundColor: [
          'rgb(0, 38, 153, .8)',
          'rgba(102, 140, 255, .8)',
          'rgba(77, 0, 0, .8)',
          'rgba(0, 102, 0, .8)',
          'rgba(179, 179, 0, .8)',

        ],
    }],

    labels: [
        'Fiction',
        'Novel',
        'Mystery',
        'Triller',
        'Horror',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


